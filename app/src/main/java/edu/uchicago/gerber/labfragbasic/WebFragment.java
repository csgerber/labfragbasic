package edu.uchicago.gerber.labfragbasic;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class WebFragment extends Fragment {


    public WebFragment() {
        // Required empty public constructor
    }


    public static WebFragment getInstance(String strUrl){

        Bundle bundle = new Bundle();
        bundle.putString("URL", strUrl);
        WebFragment webFragment = new WebFragment();
        webFragment.setArguments(bundle);

        return webFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_web, container, false);
        Bundle bundle = getArguments();
        String strUrl = bundle.getString("URL");

        WebView webView =(WebView) view.findViewById(R.id.webview);

        //http://stackoverflow.com/questions/24345834/how-to-perform-url-change-for-android-webview
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(strUrl);
        webView.setWebViewClient(new WebViewClient());

        return view;


    }

}
